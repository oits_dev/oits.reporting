﻿namespace Oits.Reporting { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportSection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class ReportSection : System.Configuration.ConfigurationSection { 

		#region fields
		/// <summary>The default name of the <see cref="Oits.Reporting.ReportSection" /> element in the configuration file.</summary>
		public const System.String DefaultSectionName = "oits.reporting";
		#endregion fields


		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportSection"]/member[@name=".#ctor"]/*'/>
		public ReportSection() : base() { 
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportSection"]/member[@name="Reports"]/*'/>
		[System.Configuration.ConfigurationProperty( "", IsDefaultCollection = true, IsRequired = false )]
		[System.Configuration.ConfigurationCollection( typeof( ReportElementCollection ), 
			AddItemName = "add", 
			ClearItemsName = "clear", 
			RemoveItemName = "remove" 
		)]
		public ReportElementCollection Reports { 
			get { 
				return (ReportElementCollection)base[ "" ];
			}
		}
		#endregion properties


		#region static methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportSection"]/member[@name="GetSection"]/*'/>
		public static ReportSection GetSection() { 
			return ( System.Configuration.ConfigurationManager.GetSection( ReportSection.DefaultSectionName ) as ReportSection );
		}
		#endregion static methods

	}

}