﻿namespace Oits.Reporting { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportElementCollection"]/member[@name=""]/*'/>
	[System.Serializable]
	public class ReportElementCollection : Oits.Configuration.NamedConfigurationElementCollectionBase<ReportElement> { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportElementCollection"]/member[@name=".#ctor"]/*'/>
		public ReportElementCollection() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportElementCollection"]/member[@name=".#ctor(System.Collections.IComparer)"]/*'/>
		public ReportElementCollection( System.Collections.IComparer comparer ) : base( comparer ) { 
		}
		#endregion .ctor

	}

}