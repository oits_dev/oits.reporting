﻿namespace Oits.Reporting { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportCredential"]/member[@name=""]/*'/>
	[System.Serializable]
	public class ReportCredential : Microsoft.Reporting.WebForms.IReportServerCredentials { 

		#region fields
		private System.String myUserName;
		private System.String myPassword;
		private System.String myDomain;
		#endregion fields


		#region .ctor
		private ReportCredential() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportCredential"]/member[@name=".#ctor(System.String,System.String,System.String)"]/*'/>
		public ReportCredential( System.String userName, System.String password, System.String domainName ) : this() { 
			this.UserName = userName;
			this.Password = password;
			this.Domain = domainName;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportCredential"]/member[@name=".#ctor(Oits.Configuration.Credential.CredentialElement)"]/*'/>
		public ReportCredential( Oits.Configuration.Credential.CredentialElement credential ) : this() { 
			if ( null == credential ) { 
				throw new System.ArgumentNullException( "credential" );
			}
			this.UserName = credential.UserName;
			this.Domain = credential.Domain;
			this.Password = credential.Password;
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportCredential"]/member[@name=".#ctor(System.Net.NetworkCredential)"]/*'/>
		public ReportCredential( System.Net.NetworkCredential credential ) : this() { 
			if ( null == credential ) { 
				throw new System.ArgumentNullException( "credential" );
			}
			this.UserName = credential.UserName;
			this.Domain = credential.Domain;
			this.Password = credential.Password;
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportCredential"]/member[@name="ImpersonationUser"]/*'/>
		public System.Security.Principal.WindowsIdentity ImpersonationUser { 
			get { 
				return null;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportCredential"]/member[@name="NetworkCredentials"]/*'/>
		public System.Net.ICredentials NetworkCredentials { 
			get { 
				return new System.Net.NetworkCredential( this.UserName, this.Password, this.Domain );
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportCredential"]/member[@name="UserName"]/*'/>
		public System.String UserName { 
			get { 
				return myUserName;
			}
			set { 
				myUserName = value ?? System.String.Empty;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportCredential"]/member[@name="Password"]/*'/>
		public System.String Password { 
			get { 
				return myPassword;
			}
			set { 
				myPassword = value ?? System.String.Empty;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportCredential"]/member[@name="Domain"]/*'/>
		public System.String Domain { 
			get { 
				return myDomain;
			}
			set { 
				myDomain = value ?? System.String.Empty;
			}
		}
		#endregion properties


		#region methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportCredential"]/member[@name="GetFormsCredentials(System.Net.Cookie@,System.String@,System.String@,System.String@)"]/*'/>
		public System.Boolean GetFormsCredentials( out System.Net.Cookie authCookie, out System.String userName, out System.String password, out System.String authority ) { 
			authCookie = null;
			userName = null;
			password = null;
			authority = null;
			return false;
		}
		#endregion methods


		#region static methods
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportCredential"]/member[@name="FromCredentialName(System.String)"]/*'/>
		public static ReportCredential FromCredentialName( System.String credentialName ) { 
			if ( System.String.IsNullOrEmpty( credentialName ) ) { 
				throw new System.ArgumentNullException( "credentialName" );
			}
			return new ReportCredential( Oits.Configuration.Credential.CredentialSection.GetSection().Credentials[ credentialName ] );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportCredential"]/member[@name="op_Explicit(System.Net.NetworkCredential)~Oits.Reporting.ReportCredential"]/*'/>
		public static explicit operator ReportCredential( System.Net.NetworkCredential credential ) { 
			if ( null == credential ) { 
				throw new System.ArgumentNullException( "credential" );
			}
			return new ReportCredential( credential );
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportCredential"]/member[@name="op_Explicit(Oits.Configuration.Credential.CredentialElement)~Oits.Reporting.ReportCredential"]/*'/>
		public static explicit operator ReportCredential( Oits.Configuration.Credential.CredentialElement credential ) { 
			if ( null == credential ) { 
				throw new System.ArgumentNullException( "credential" );
			}
			return new ReportCredential( credential.ToNetworkCredential() );
		}
		#endregion static methods

	}

}