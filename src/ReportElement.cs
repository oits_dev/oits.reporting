﻿namespace Oits.Reporting { 

	/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportElement"]/member[@name=""]/*'/>
	[System.Serializable]
	public class ReportElement : System.Configuration.ConfigurationElement, Oits.Configuration.INamedConfigurationElement { 

		#region .ctor
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportElement"]/member[@name=".#ctor"]/*'/>
		public ReportElement() : base() { 
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportElement"]/member[@name=".#ctor(System.String)"]/*'/>
		public ReportElement( System.String name ) : this() { 
			this.Name = name;
		}
		#endregion .ctor


		#region properties
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportElement"]/member[@name="Name"]/*'/>
		[System.Configuration.ConfigurationProperty( "name", DefaultValue = "", IsRequired = true, IsKey = true )]
		public System.String Name { 
			get { 
				return (System.String)base[ "name" ];
			}
			set { 
				base[ "name" ] = value;
			}
		}

		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportElement"]/member[@name="Uri"]/*'/>
		[System.Configuration.ConfigurationProperty( "uri", DefaultValue = (System.String)null, IsRequired = true, IsKey = false )]
		public System.Uri Uri { 
			get { 
				return (System.Uri)base[ "uri" ];
			}
			set { 
				base[ "uri" ] = value;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportElement"]/member[@name="Path"]/*'/>
		[System.Configuration.ConfigurationProperty( "path", DefaultValue = (System.String)null, IsRequired = true, IsKey = false )]
		public System.String Path { 
			get { 
				return (System.String)base[ "path" ];
			}
			set { 
				base[ "path" ] = value;
			}
		}
		/// <include file='.\Doc\XmlDoc.xml' path='/types/type[@name="Oits.Reporting.ReportElement"]/member[@name="CredentialName"]/*'/>
		[System.Configuration.ConfigurationProperty( "credentialName", DefaultValue = (System.String)null, IsRequired = false, IsKey = false )]
		public System.String CredentialName { 
			get { 
				return (System.String)base[ "credentialName" ];
			}
			set { 
				base[ "credentialName" ] = value;
			}
		}
		#endregion properties

	}

}